import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./pages/login/login.component";
import {SalleListeComponent} from "./pages/salle-liste/salle-liste.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'roomlist', component: SalleListeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
