import { Component, OnInit } from '@angular/core';
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-salle-liste',
  templateUrl: './salle-liste.component.html',
  styleUrls: ['./salle-liste.component.css'],
  standalone: true,
  imports: [CommonModule]
})
export class SalleListeComponent implements OnInit {
  salles = [
    {
      nom: 'Salle A',
      image: 'chemin/vers/image.jpg',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      equipements: ['Télévision', 'Table', '20 places', 'Tableau'],
      places: 20,
      statut: 'Disponible'
    },
    // Ajoutez d'autres salles ici
  ];

  ngOnInit() {
  }
}
